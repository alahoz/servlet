package modelos;

public class Autorizacion {

	private String idUsuario;
	private String idClave;
	private String codigo;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdClave() {
		return idClave;
	}
	public void setIdClave(String idClave) {
		this.idClave = idClave;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
