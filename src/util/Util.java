package util;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bd.AutorizacionDAO;
import bd.ClaveApiDAO;
import modelos.ClaveAPI;

public class Util {

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
	private static final String[] LIST_URL = {"/login", "/usuario"};
	private static final String[][] METHODS=  { {"POST"}, {"GET"} };
	
	public static <T> T leerJSON(String json, Class<T> c) {
		return gson.fromJson(json, c);
	}
    
	public static <T> T leerJSON(String json, Type t) {
		return gson.fromJson(json, t);
	}
    
	public static void escribirJSON(HttpServletResponse response, Object obj) throws IOException {
		String json = gson.toJson(obj);
		response.setContentType("application/json");
		response.getWriter().write(json);
	}

	public static DatosPeticion comprobarSeguridad(HttpServletRequest request, HttpServletResponse response) throws IOException {
		DatosPeticion dp = new DatosPeticion();
		dp.setCodigoCliente(request.getHeader("X-CodigoCliente"));
		String signature = request.getHeader("X-Signature");
		String autorizacion = request.getHeader("X-Autorizacion");
		String timestamp = request.getHeader("X-Timestamp");
		if (dp.getCodigoCliente() == null || !ClaveApiDAO.carga(dp.getCodigoCliente())) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return null;
		}
		if ((autorizacion == null || !AutorizacionDAO.existeAutorizacion(autorizacion)) || !request.getServletPath().contentEquals("/login")) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return null;
		}
		if (timestamp == null || hasCorrectTimestamp(timestamp)) {
			response.setStatus(412);
			return null;
		}
		
		if (methodIsNotAcceptable(request)) {
			response.setStatus(405);
		}

		// Construir mensaje para calcular firma
		StringBuilder sb = new StringBuilder();
		// Linea 1 - Metodo + URL
		sb.append(request.getMethod()).append(" ");
		sb.append(request.getServletPath());
		if (request.getPathInfo() != null) {
			sb.append(request.getPathInfo());
		}
		sb.append("\n");
		// Linea 2 - Codigo Cliente
		sb.append(dp.getCodigoCliente()).append("\n");
		// Linea 3 - Contenido petición
		if ("POST".contentEquals(request.getMethod()) ||
			"PUT".contentEquals(request.getMethod())) {
			dp.setJson(leerReader(request.getReader()));
			sb.append(dp.getJson()).append("\n");
		}
		
		/*String firma = Cripto.calcularMac(sb.toString(), cliente.getClave());
		if (!signature.equals(firma)) {
			response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
			return null;	
		}*/
				
		return dp;
	}
	
	public static boolean methodIsNotAcceptable(HttpServletRequest request) {
		String currentURL = request.getServletPath();
		for (int i = 0; i < LIST_URL.length; i++) {
			if(LIST_URL[i].equals(currentURL)) {
				for (String method : METHODS[i]) {
					if(request.getServletPath().equals(method)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public static boolean hasCorrectTimestamp(String timestamp) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.clear();
		calendar.set(1970, Calendar.JANUARY, 1);
		long miliseconds = calendar.getTimeInMillis();
		return miliseconds - Integer.parseInt(timestamp) <= 300000;
	}
	
	public static String leerReader(Reader reader) throws IOException {
		StringBuilder sb = new StringBuilder();
		char[] chars = new char[1024];
		int n;
		while ((n = reader.read(chars)) > 0) {
			sb.append(chars, 0, n);
		}
		return sb.toString();
	}

	

}
