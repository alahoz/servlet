package services;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bd.AutorizacionDAO;
import bd.UsuarioDAO;
import modelos.Autorizacion;
import modelos.Usuario;
import util.Cripto;
import util.DatosPeticion;
import util.Util;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/usuario")
public class UsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public UsuarioServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BufferedReader br = request.getReader();
		String strProvisional;
		Gson gs = new Gson();
		String payload = "";
		while ((strProvisional = br.readLine())!= null) {
		    payload += strProvisional;
		}
		Autorizacion a = gs.fromJson(payload, Autorizacion.class);
		Usuario u = AutorizacionDAO.getUsuarioByAutorizacion(a.getCodigo());
		Util.escribirJSON(response, u);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BufferedReader br = request.getReader();
		String strProvisional;
		Gson gs = new Gson();
		String payload = "";
		while ((strProvisional = br.readLine())!= null) {
		    payload += strProvisional;
		}
		Usuario u = gs.fromJson(payload, Usuario.class);
		String codigoAutorizacion = UsuarioDAO.login(u.getLogin(), u.getPwd());
		codigoAutorizacion = Cripto.cifrarClave(codigoAutorizacion);
		Util.escribirJSON(response, codigoAutorizacion);
	}

}
