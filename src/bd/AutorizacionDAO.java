package bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import modelos.Usuario;

public class AutorizacionDAO {

	public static String getCodigoAutorizacion(int idUsuario) {
		try {
			Connection c = BD.abrirBD();
			PreparedStatement st = c.prepareStatement("select codigo from autorizaciones where idUsuario=?");
			st.setInt(1, idUsuario);
			ResultSet rs = st.executeQuery();
			String codigo = "";
			if (rs.next()) {
				codigo = rs.getString("codigo");
			}
			rs.close();
			st.close();
			c.close();
			return codigo;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public static boolean existeAutorizacion(String autorizacion) {
		try {
			Connection c = BD.abrirBD();
			PreparedStatement st = c.prepareStatement("select idUsuario from autorizaciones where codigo=?");
			st.setString(1, autorizacion);
			ResultSet rs = st.executeQuery();
			int idUsuario = 0;
			if (rs.next()) {
				idUsuario = rs.getInt("idUsuario");
			}
			rs.close();
			st.close();
			c.close();
			return idUsuario != 0;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Usuario getUsuarioByAutorizacion(String autorizacion) {
		try {
			Connection c = BD.abrirBD();
			PreparedStatement st = c.prepareStatement("select usuarios.nombre, usuarios.login from autorizaciones INNER JOIN usuarios on usuarios.id = autorizaciones.idUsuario "
					+ "where codigo = ?");
			st.setString(1, autorizacion);
			ResultSet rs = st.executeQuery();
			Usuario u = new Usuario();
			if (rs.next()) {
				u.setNombre(rs.getString("nombre"));
				u.setLogin(rs.getString("login"));
			}
			rs.close();
			st.close();
			c.close();
			return u;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
