package bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import bd.BD;
import modelos.ClaveAPI;

public class ClaveApiDAO {
	public static boolean carga(String codigo) {
		try {
			Connection c = BD.abrirBD();
			PreparedStatement st = c.prepareStatement("select id from clavesapi where codigo=?");
			st.setString(1, codigo);
			ResultSet rs = st.executeQuery();
			int id = 0;
			if (rs.next()) {
				id = rs.getInt("id");
			}
			rs.close();
			st.close();
			c.close();
			return id != 0;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
